$keyPath = "a:\vagrant.pub"
$destination = "C:\Users\vagrant\.ssh\authorized_keys"
if (Test-Path $keyPath) {
    Copy-Item -Path $keyPath -Destination $destination
} else {
    $Tls12 = [System.Net.SecurityProtocolType]::Tls12
    [System.Net.ServicePointManager]::SecurityProtocol = $Tls12
    $vagrant_download_url = "https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub"
    Write-Output "Downloading $vagrant_download_url"
    (New-Object System.Net.WebClient).DownloadFile($vagrant_download_url, "C:\Users\vagrant\.ssh\authorized_keys")
}